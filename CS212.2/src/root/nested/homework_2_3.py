# --------------
# User Instructions
#
# Write a function, longest_subpalindrome_slice(text) that takes 
# a string as input and returns the i and j indices that 
# correspond to the beginning and end indices of the longest 
# palindrome in the string. 
#
# Grading Notes:
# 
# You will only be marked correct if your function runs 
# efficiently enough. We will be measuring efficency by counting
# the number of times you access each string. That count must be
# below a certain threshold to be marked correct.
#
# Please do not use regular expressions to solve this quiz!

def palindrome(text, pos, even= False):
    l= len(text)-1
    lo= pos
    if even:
        hi= pos
    else:
        hi= pos+1
    while lo>0 and hi<=l:
        if text[lo-1] == text[hi]:
            lo-=1
            hi+=1
        else:
            break
    return (lo, hi)


def longest_subpalindrome_slice(text):
    "Return (i, j) such that text[i:j] is the longest palindrome in text."
    lo= hi= 0
    t= text.upper()
    for pos in range(len(t)):
        (l, h)= palindrome(t, pos, False)
        if (h-l) > (hi-lo):
            (lo, hi)= (l,h)
        (l, h)= palindrome(t, pos, True)
        if (h-l) > (hi-lo):
            (lo, hi)= (l,h)
    return (lo, hi)

    
def test():
    L = longest_subpalindrome_slice
    assert L('racecar') == (0, 7)
    assert L('Racecar') == (0, 7)
    assert L('RacecarX') == (0, 7)
    assert L('Race carr') == (7, 9)
    assert L('') == (0, 0)
    assert L('something rac e car going') == (8,21)
    assert L('xxxxx') == (0, 5)
    assert L('Mad am I ma dam.') == (0, 15)
    return 'tests pass'

print test()