'''
document use of 

Created on Oct 19, 2012

@author: cmaier
'''
import string, re, itertools
import time

def solve(formula):
    """Given a formula like 'ODD + ODD == EVEN', fill in digits to solve it.
    Input formula is a string; output is a digit-filled-in string or None."""
    for f in fill_in(formula):
        if valid(f):
            return f
    
def fill_in(formula):
    "Generate all possible fillings-in of letters in formula with digits."
    #letters = ''.join(set(re.findall('[A-Z]', formula)))
    letters = ''.join([c for c in string.ascii_letters if c in formula])
    for digits in itertools.permutations('1234567890', len(letters)):
        table = string.maketrans(letters, ''.join(digits))
        yield formula.translate(table)
    
def valid(f):
    """Formula f is valid if and only if it has no 
    numbers with leading zero, and evals true."""
    try: 
        return not re.search(r'\b0[0-9]', f) and eval(f) is True
    except ArithmeticError:
        return False

def timedcall(fn, *args):
    "Call function with args; return the time in seconds and result."
    t0 = time.clock()
    result = fn(*args)
    t1 = time.clock()
    return t1-t0, result

def test(examples):
    t0= time.clock()
    for example in examples:
        print; print 13*' ', example
        print '%6.4f sec:   %s' % timedcall(solve, example)
    print '%6.4f total' % (time.clock()-t0)

examples='''TWO + TWO == FOUR
ONE < TWO < THREE
ODD + ODD == EVEN
PLUTO not in set([PLANETS])'''.splitlines()

# from command line one could do
#$ python -m cProfile unit_2_36.py
# but we can also do
import cProfile
cProfile.run('test(examples)')
