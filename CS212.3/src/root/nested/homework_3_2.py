# --------------
# User Instructions
#
# Write a function, inverse, which takes as input a monotonically
# increasing (always increasing) function that is defined on the 
# non-negative numbers. The runtime of your program should be 
# proportional to the LOGARITHM of the input. You may want to 
# do some research into binary search and Newton's method to 
# help you out.
#
# This function should return another function which computes the
# inverse of the input function. 
#
# Your inverse function should also take an optional parameter, 
# delta, as input so that the computed value of the inverse will
# be within delta of the true value.

# -------------
# Grading Notes
#
# Your function will be called with three test cases. The 
# input numbers will be large enough that your submission
# will only terminate in the allotted time if it is 
# efficient enough. 

def slow_inverse(f, delta=1/128.):
    """Given a function y = f(x) that is a monotonically increasing function on
    non-negatve numbers, return the function x = f_1(y) that is an approximate
    inverse, picking the closest value to the inverse, within delta."""
    def f_1(y):
        x = 0
        while f(x) < y:
            x += delta
        # Now x is too big, x-delta is too small; pick the closest to y
        return x if (f(x)-y < y-f(x-delta)) else x-delta
    return f_1 

def inverse(f, delta = 1/128.):
    """Given a function y = f(x) that is a monotonically increasing function on
    non-negatve numbers, return the function x = f_1(y) that is an approximate
    inverse, picking the closest value to the inverse, within delta."""
    def perverse(y):
        xl = 2.
        xh = 4.
        while (y-f(xl))*(y-f(xh)) > 0:
            if y-f(xh) > 0:
                xl = xh
                xh *= 2
            if y-f(xl) < 0:
                xh = xl
                xl /= 2
        if xh != xl:
            while True:
                x = 0.5*(xh+xl)
                yy= f(x)
                if yy > y:
                    xh = x
                else:
                    xl = x
                if xh - xl < delta:
                    break
        else:
            x = xl
        return x
    return perverse
    
def square(x): return x*x
sqrt = slow_inverse(square)

#import cProfile

#for func in ('sqrt(1000000000)', 'inverse(square)(1000000000)', 'inverse(lambda x : 10**x)(100000)'):
#    cProfile.run(func)

#print sqrt(1000000000)
#print inverse(square)(1000000000)
#print inverse(lambda x : 10**x)(100000)
